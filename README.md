# Gestion automatisée de la publication de métadonnées

## Utilisation
Pour lancer le notebook, 3 options : 

+ [utiliser mybinder (sans installation sur votre machine)](#avec-mybinder)
+ [utiliser votre machine (impliquant une installation complète de l'environnement sur votre machine)](#sur-votre-machine)
+ [Utiliser le serveur Jupyterhub de TETIS (uniquement possible sur l'intranet de la maison de la télédétection)](#sur-le-serveur-jupyterhub-de-tetis)
### Avec MyBinder
Ce notebook a été conçu pour être aussi utilisé via [mybinder](https://mybinder.org/).
Pour cela, cliquez sur l'image : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.irstea.fr%2Fumr-tetis%2Fidg-tetis%2Fformation%2Fpublication_automatique_metadonnees%2Fautomatisation_publication_metadonnees/master)

Ou bien rendez-vous sur le site, sélectionné un dépôt git et copiez-collez l'[URL de ce dépôt](https://gitlab.irstea.fr/umr-tetis/idg-tetis/formation/publication_automatique_metadonnees/automatisation_publication_metadonnees)

### Sur votre machine
Pour pouvoir utiliser ce notebook directement depuis votre machine, voici les pré-requis à installer :

1. Installation de python3.7
2. Installation de R
3. Installation de Jupyter notebook :
    * `pip3 install jupyter`
    * `pip3 install --upgrade nbconvert`
4. Ajout de l'environnement R au jupyter notebook
    * Lancement console R : `R`
    * Installation d'un kernel pour jupyter : `install.packages('IRkernel')`
    * Association du kernel avec le jupyter : ` IRkernel::installspec()`
    * Quitter la console R : `q()`. Sauvegarder votre session
5. Lancement du notebook
    * `jupyter notebook`

### Sur le serveur jupyterhub de TETIS
1. Se rendre sur le [jupyterhub de TETIS](http://cin-mo-jupyterhub.montpellier.irstea.priv:8000/)
2. Dans le navigateur de fichier dans son espace jupyterlab, aller dans le reprtoire : `automatisation_publication_metadonnees`
3. Lancer le notebook : cliquer sur main.ipynb
